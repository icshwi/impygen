impygen
=======

Python package for generating ipmimanager_-compatible database and OPI files.
To generate files ``impygen`` uses JSON files with MTCA.4 chassis configuration.
One can produce the JSON config with a standalone tool from ipmimanager_ module.

.. _ipmimanager: https://gitlab.esss.lu.se/e3/wrappers/communication/e3-ipmimanager

Prerequisites
-------------
* Python 3.6 or higher

Quickstart
----------

Install
~~~~~~~
.. code-block:: sh

    $ pip install --upgrade pip
    $ pip install .
    $ impygen -h

Usage
~~~~~
One can find an example of a JSON file in /impygen/tests/inputs/test_full_crate.json link_.

.. _link: https://gitlab.esss.lu.se/icshwi/impygen/-/blob/main/impygen/tests/inputs/test_full_crate.json

Generate database files using JSON file with saved MTCA.4 configuration:

.. code-block:: sh

    $ impygen db <path_to_json_file>

Generate OPI files:

.. code-block:: sh

    $ impygen opi <path_to_json_file>


Configuration
-------------
By default generated database files and OPIs use ``$(P)$(R)`` macros as PVs prefixes while names of sensors are made of their real name read from MCH. One can change this behaviour.

To use sensor numbers instead of their names:

.. code-block:: sh

    $ impygen db <path_to_json_file> -n controllers_as_subsystems
    $ impygen opi <path_to_json_file> -n controllers_as_subsystems

To use advanced macros definitions ``$(P)${BOARD_TYPE}-$(ID)${OPT}Property``:

.. code-block:: sh

    $ impygen db <path_to_json_file> -m single_prefix
    $ impygen opi <path_to_json_file> -m single_prefix

One can also point out where generated files will be saved:

.. code-block:: sh

    $ impygen db <path_to_json_file> -o --path <path_for_saving>
    $ impygen opi <path_to_json_file> -o --path <path_for_saving>
