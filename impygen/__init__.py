import argparse
import json
import sys
from pathlib import Path

import pkg_resources

from impygen import opi
from impygen.db.mtca import MtcaChassis
from impygen.styles import MacrosStyle, NamingStyle


def generate_opis(
    path: Path, data: dict, naming: NamingStyle, macros: MacrosStyle
) -> None:
    crate = MtcaChassis(
        data,
        save_db_dir=path,
        naming_style=naming,
        macros_style=macros,
    )
    opis = opi.Opi(save_dir=path, boards=crate.boards)
    try:
        opis.create_dirs()
    except PermissionError:
        print(
            f"You don't seem to have the rights to create folders in {path.absolute()}"
        )
        sys.exit(-1)
    opis.copy_common_opis()
    opis.generate_board_opi()
    opis.generate_sensor_opis()


def generate_dbs(
    path: Path, data: dict, naming: NamingStyle, macros: MacrosStyle
) -> None:
    crate = MtcaChassis(
        data,
        save_db_dir=path,
        naming_style=naming,
        macros_style=macros,
    )
    try:
        path.mkdir(parents=True, exist_ok=True)
    except PermissionError:
        print(f"You don't seem to have the rights to create folder {path.absolute()}")
        sys.exit(-1)
    crate.fill_dbs()


def generate_files():
    parser = argparse.ArgumentParser(
        description="Tool to generate DBs and OPIs based on the json with MTCA chassis configuration"
    )
    version = pkg_resources.get_distribution("impygen").version
    parser.add_argument("--version", action="version", version=version)

    base_subparser = argparse.ArgumentParser(add_help=False)
    base_subparser.add_argument(
        "input", help="path to input file with json", type=argparse.FileType()
    )
    base_subparser.add_argument(
        "--naming",
        "-n",
        type=NamingStyle.from_string,
        help="use sensor numbers instead of sensors names",
        choices=list(NamingStyle),
        default=NamingStyle.CONTROLLERS_AS_DEVICES,
    )
    base_subparser.add_argument(
        "--macros",
        "-m",
        type=MacrosStyle.from_string,
        help="use non-standard macros (modules names with $(ID) macro) in db files",
        choices=list(MacrosStyle),
        default=MacrosStyle.SEPARATED_PREFIX,
    )

    subparsers = parser.add_subparsers(title="actions", dest="action")
    db_parser = subparsers.add_parser(
        "db", help="create DB files", parents=[base_subparser]
    )
    opi_parser = subparsers.add_parser(
        "opi", help="create OPIs", parents=[base_subparser]
    )

    opi_parser.add_argument(
        "--path",
        "-p",
        help="path where to save OPI files",
        type=Path,
        default=Path("auto-gen-opi"),
    )

    db_parser.add_argument(
        "--path",
        "-p",
        help="path where to save DB files",
        type=Path,
        default=Path("auto-gen-db"),
    )

    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        sys.exit(-1)

    args = parser.parse_args()

    data = json.load(args.input)

    if args.action == "db":
        generate_dbs(args.path, data, args.naming, args.macros)
    elif args.action == "opi":
        generate_opis(args.path, data, args.naming, args.macros)
