# -*- coding: utf-8 -*-

"""This module is used to generate database file corresponsding to board configuration.

The board configuration is specified in loaded JSON file.
"""

import json
import re
import shutil
from pathlib import Path

from impygen.db.sensors import Sensor
from impygen.styles import MacrosStyle, NamingStyle


class Board:
    types = {
        "MTCA": (64, 66),
        "CU": (47, 50),
        "PM": (30, 35),
        "$(BOARD_TYPE=RTM)": (16, 29),
    }
    opts = {
        "MCH-PCIe": "PCI-",
        "MCH-Clock": "Clk-",
    }

    def __init__(
        self,
        data: dict,
        *,
        naming_style: NamingStyle,
        macros_style: MacrosStyle,
        db_path: Path = Path.cwd(),
    ) -> None:
        """Board class initialization with board data and naming styles."""
        self.root = Path(__file__).parent.parent
        path_file = Path(self.root, "stylesheets", "paths.json")
        self.paths = json.loads(path_file.read_text())
        self.db_dir = db_path
        self.macros_style = macros_style
        self.name = data["fruname"].replace(" ", "")
        self.slot = data["slot"]
        self.file_name = re.sub("[^A-Za-z0-9]+", "_", self.name)
        self.sensors = [
            Sensor(s, naming_style=naming_style, macros_style=macros_style)
            for s in data["sensors"]
        ]

    @property
    def opt(self) -> str:
        return Board.opts.get(self.name, "")

    @property
    def type_(self) -> str:
        for name, _range in Board.types.items():
            if int(self.slot) in range(*_range):
                return name
        return "$(BOARD_TYPE=AMC)"

    def _create_dbs(self) -> Path:
        """Creates DB files.

        This function creates DB files using board name and adding its slot to it.
        It also copies board template (depending on the macros style) to the newly created DB file.
        """
        name = f"{self.file_name}_slot{self.slot}.db"
        dst = self.db_dir / name

        if self.macros_style == MacrosStyle.SINGLE_PREFIX:
            shutil.copy(self.root / self.paths["boards"]["board_template"], dst)
        elif self.macros_style == MacrosStyle.SEPARATED_PREFIX:
            shutil.copy(self.root / self.paths["boards"]["pr_board_template"], dst)

        return dst

    def _replace_macros(self, db_file_name: Path) -> None:
        """Replaces macros from templates."""
        s = db_file_name.read_text()
        substitutions = {
            "${BOARD_NAME}": self.name,
            "${OPT}": self.opt,
            "${BOARD_TYPE}": self.type_,
        }
        for initial, replacement in substitutions.items():
            s = s.replace(initial, replacement)
        db_file_name.write_text(s)

    def fill_dbs(self) -> None:
        """Creates DB files and fills them with proper templates.

        This function calls all neded methods to create DB files
        """
        db = self._create_dbs()
        for s in self.sensors:
            s.add_sensors(db_file_name=db)
            s.add_thresholds(db_file_name=db)
            s.replace_macros(db_file_name=db)
        self._replace_macros(db_file_name=db)
