# -*- coding: utf-8 -*-

"""This module is used to create empty database files for MTCA.4 boards.

The boards are listed in MTCA.4 JSON configuration file.
The module contains methods to fill those files with board and sensor PVs.
"""

from pathlib import Path
from typing import List

from impygen.db.board import Board
from impygen.styles import MacrosStyle, NamingStyle


class MtcaChassis:
    def __init__(
        self,
        ipmi_json: dict,
        *,
        save_db_dir: Path,
        naming_style: NamingStyle,
        macros_style: MacrosStyle,
    ) -> None:
        """MtcaChassis class initialization with MTCA.4 data and naming styles."""
        self.boards: List[Board] = [
            Board(
                b,
                naming_style=naming_style,
                macros_style=macros_style,
                db_path=save_db_dir,
            )
            for b in ipmi_json["utca"]["boards"]
        ]

    def fill_dbs(self) -> None:
        """Creates DB files and fills them with proper templates.

        This function calls all neded methods to create DB files
        """
        for b in self.boards:
            b.fill_dbs()
