# -*- coding: utf-8 -*-

"""This module is used to add to DB files PVs corresponding to sensor parameters.

The sensor configurations are loaded from MTCA.4 JSON file. It adds sensor PVs,
chooses which threshold template should be used. Finally it replaces sensor macros.
"""

import json
import re
from pathlib import Path

from impygen.styles import MacrosStyle, NamingStyle


class Sensor:
    def __init__(
        self, data: dict, *, naming_style: NamingStyle, macros_style: MacrosStyle
    ) -> None:
        """Sensor class initialization with sensor data and naming styles."""
        self.root = Path(__file__).parent.parent
        path_file = Path(self.root, "stylesheets", "paths.json")
        self.paths = json.loads(path_file.read_text())
        self.naming_style = naming_style
        self.macros_style = macros_style
        self.name = re.sub("[^A-Za-z0-9]+", "", data["name"])
        self.unit = self._format_unit(data["units"])
        self.id = data["index"]
        self.thresh_access = int(data["thrAccess"], 0)
        self.thresh_update = int(data["thrUpdateAllowed"], 16)

    def _format_unit(self, unit: str) -> str:
        """Return proper unit format."""
        return {
            "volts": "V",
            "amps": "A",
            "C": "degC",
            "RPM": "rpm",
            "kbytes": "kB",
        }.get(unit, unit)

    def add_sensors(self, *, db_file_name: Path) -> None:
        """Add sensor PVs.

        This function depending on the macros style chooses PV template and
        copies it to newly created DB file. Then, it replaces macros with
        sensor parameters.
        """
        if self.macros_style == MacrosStyle.SINGLE_PREFIX:
            template = self.root / self.paths["sensors"]["sensor_template"]
        elif self.macros_style == MacrosStyle.SEPARATED_PREFIX:
            template = self.root / self.paths["sensors"]["pr_sensor_template"]

        with open(db_file_name, "a") as target, open(template, "r") as src:
            for line in src:
                if self.naming_style == NamingStyle.CONTROLLERS_AS_SUBSYSTEMS:
                    line = line.replace("${SENSOR_NAME}", "Sensor" + str(self.id))
                elif self.naming_style == NamingStyle.CONTROLLERS_AS_DEVICES:
                    line = line.replace("${SENSOR_NAME}", self.name)
                line = line.replace("${SINDEX}", str(self.id))
                line = line.replace("${UNIT}", self.unit)
                line = line.replace("${THR_ACCESS}", str(self.thresh_access))
                target.write(line)

    def _create_thresh_file(
        self,
        *,
        db_file_name: Path,
        template_name: Path,
        threshold_name: str,
        description: str,
    ) -> None:
        """Adds threshold PVs.

        This function adds threshold PVs to sensor DB file.
        """
        with open(db_file_name, "a") as target, open(template_name, "r") as src:
            for line in src:
                line = line.replace("${THRESHOLD_NAME}", threshold_name)
                line = line.replace("${THRESHOLD_DESC}", description)
                target.write(line)

    def _get_thresh_template(self) -> Path:
        """Return path to proper template.

        This function decides which thresholds templates should
        be added to sensor PVs. It is based on the sensor
        threshold access and theshold update allowed values.
        """
        # thresholds are readable per reading mask
        if self.thresh_access == 1:
            if self.macros_style == MacrosStyle.SINGLE_PREFIX:
                template = self.root / self.paths["sensors"]["not_mod_thresh_template"]
            elif self.macros_style == MacrosStyle.SEPARATED_PREFIX:
                template = (
                    self.root / self.paths["sensors"]["pr_not_mod_thresh_template"]
                )
        # thresholds are readable and settable per reading mask ans settable threshold mask
        elif self.thresh_access == 2:
            if self.macros_style == MacrosStyle.SINGLE_PREFIX:
                template = self.root / self.paths["sensors"]["mod_thresh_template"]
            elif self.macros_style == MacrosStyle.SEPARATED_PREFIX:
                template = self.root / self.paths["sensors"]["pr_mod_thresh_template"]
        else:
            raise NotImplementedError
        return template

    def _load_thresh_template(self, *, db_name: Path) -> None:
        """Configures threshold values.

        This function configures threshold parameters that
        will be used when the threhold template will be added
        to DB file.
        """
        try:
            template = self._get_thresh_template()
        except NotImplementedError:
            return

        # lower non critical
        if (self.thresh_update & 0x01) != 0:
            thresh_name = "ThrLoNonCrt"
            desc = "threshold low non-critical"
            self._create_thresh_file(
                db_file_name=db_name,
                template_name=template,
                threshold_name=thresh_name,
                description=desc,
            )
        # lower critical
        if (self.thresh_update & 0x02) != 0:
            thresh_name = "ThrLoCrt"
            desc = "threshold low critical"
            self._create_thresh_file(
                db_file_name=db_name,
                template_name=template,
                threshold_name=thresh_name,
                description=desc,
            )
        # lower non recoverable
        if (self.thresh_update & 0x04) != 0:
            thresh_name = "ThrLoNonRec"
            desc = "threshold low non-recoverable"
            self._create_thresh_file(
                db_file_name=db_name,
                template_name=template,
                threshold_name=thresh_name,
                description=desc,
            )
        # upper non critical
        if (self.thresh_update & 0x08) != 0:
            thresh_name = "ThrUpNonCrt"
            desc = "threshold upper non-critical"
            self._create_thresh_file(
                db_file_name=db_name,
                template_name=template,
                threshold_name=thresh_name,
                description=desc,
            )
        # upper critical
        if (self.thresh_update & 0x10) != 0:
            thresh_name = "ThrUpCrt"
            desc = "threshold upper critical"
            self._create_thresh_file(
                db_file_name=db_name,
                template_name=template,
                threshold_name=thresh_name,
                description=desc,
            )
        # upper non recoverable
        if (self.thresh_update & 0x20) != 0:
            thresh_name = "ThrUpNonRec"
            desc = "threshold upper non-recoverable"
            self._create_thresh_file(
                db_file_name=db_name,
                template_name=template,
                threshold_name=thresh_name,
                description=desc,
            )

    def add_thresholds(self, *, db_file_name: Path) -> None:
        """Decides if threshold PVs should be added."""
        # 0 - no thresholds
        # 3 - fixed, unreadable thresholds
        if self.thresh_access == 0 or self.thresh_access == 3:
            return
        # threshold access 1 or 2
        self._load_thresh_template(db_name=db_file_name)

    def replace_macros(self, db_file_name: Path) -> None:
        """Replaces macros with sensor parameters."""
        s = db_file_name.read_text()

        if self.naming_style == NamingStyle.CONTROLLERS_AS_SUBSYSTEMS:
            s = s.replace("${SENSOR_NAME}", "Sensor" + str(self.id))
        elif self.naming_style == NamingStyle.CONTROLLERS_AS_DEVICES:
            s = s.replace("${SENSOR_NAME}", self.name)

        s = s.replace("${SINDEX}", str(self.id))
        s = s.replace("${UNIT}", self.unit)
        db_file_name.write_text(s)
