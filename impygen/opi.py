# -*- coding: utf-8 -*-

"""This module is used to generate OPerator Interfaces (OPIs) for MTCA.4

The configuration is specified in loaded JSON file.
"""

import json
import math
import shutil
from pathlib import Path
from typing import List

from phoebusgen import colors, fonts, screen, widget

from impygen.db.board import Board


class Opi:
    DIRS = ["sensors", "expert", "fru", "thresholds", "archiver"]

    def __init__(self, *, save_dir: Path, boards: List[Board]) -> None:
        """Opi class initialization with boards and Path to DB file."""
        self.root = Path(__file__).parent
        board_file = self.root / "stylesheets" / "board_config.json"
        self.board_config = json.loads(board_file.read_text())
        sensor_file = self.root / "stylesheets" / "sensor_config.json"
        self.sensor_config = json.loads(sensor_file.read_text())
        colors_file = self.root / "stylesheets" / "colors.json"
        self.colors = json.loads(colors_file.read_text())
        self.boards = boards
        self.save_path = save_dir
        self.opi_name = "boards.bob"
        self.board_screen = screen.Screen(self.board_config["screen_name"])
        path_file = self.root / "stylesheets" / "paths.json"
        self.paths = json.loads(path_file.read_text())

    def copy_common_opis(self) -> None:
        """Copy common OPIs from template dir."""
        for _, value in self.paths["opis"].items():
            shutil.copy(self.root / value["src"], self.save_path / value["dst"])
        self._copy_images()

    def _copy_images(self) -> None:
        """Copy images used in threshold window."""
        shutil.copytree(
            self.root / self.paths["images"],
            self.save_path / "images",
        )

    def create_dirs(self) -> None:
        """Create folders for OPI files."""
        for d in self.DIRS:
            path = self.save_path / d
            path.mkdir(parents=True, exist_ok=True)

    def generate_sensor_opis(self) -> None:
        """Generates OPIs for sensors for specified board.

        This function iterates through list of Board objects and
        for each configuration it creates a new window with sensors.
        """
        for b in self.boards:
            sensor_screen = screen.Screen(self.sensor_config["screen_name"])
            sensor_opi_name = f"{b.file_name}_slot{b.slot}.bob"
            self._add_sensor_static_elements(sensor_screen, b)
            sensor_screen.write_screen(self.save_path / "sensors" / sensor_opi_name)

    def _add_line_color(self, widget: widget) -> None:
        """Configures colors of border lines.

        This functions uses dict with colors (configuration from JSON file)
        and set each value of RGB value.
        """
        widget.line_color(
            self.colors["rectangle"]["line"]["r"],
            self.colors["rectangle"]["line"]["g"],
            self.colors["rectangle"]["line"]["b"],
        )

    def _add_rect_color(self, widget: widget) -> None:
        """Configures rectangle's background color.

        This functions uses dict with colors (configuration from JSON file)
        and set each value of RGB value.
        """
        widget.background_color(
            self.colors["rectangle"]["background"]["r"],
            self.colors["rectangle"]["background"]["g"],
            self.colors["rectangle"]["background"]["b"],
        )

    def _add_group_color(self, widget: widget) -> None:
        """Configures group's background color.

        This functions uses dict with colors (configuration from JSON file)
        and set each value of RGB value.
        """
        widget.background_color(
            self.colors["group_background"]["r"],
            self.colors["group_background"]["g"],
            self.colors["group_background"]["b"],
        )

    @staticmethod
    def _add_spinner(config: dict) -> widget.Spinner:
        """Return spinner widget."""
        return widget.Spinner(
            config["name"],
            config["pv"],
            config["x"],
            config["y"],
            config["width"],
            config["height"],
        )

    @staticmethod
    def _add_group(config: dict, y: int = -1, height: int = -1) -> widget.Group:
        """Returns group widget."""
        if height != -1:
            set_h = height
        else:
            set_h = config["height"]
        if y != -1:
            set_y = y
        else:
            set_y = config["y"]
        return widget.Group(
            config["name"],
            config["x"],
            set_y,
            config["width"],
            set_h,
        )

    @staticmethod
    def _add_update(config: dict) -> widget.TextUpdate:
        """Returns update text widget."""
        return widget.TextUpdate(
            config["name"],
            config["pv"],
            config["x"],
            config["y"],
            config["width"],
            config["height"],
        )

    @staticmethod
    def _add_rect(config: dict) -> widget.Rectangle:
        """Returns update text widget."""
        return widget.Rectangle(
            config["name"],
            config["x"],
            config["y"],
            config["width"],
            config["height"],
        )

    @staticmethod
    def _add_label(config: dict) -> widget.Rectangle:
        """Returns label widget."""
        return widget.Label(
            config["name"],
            config["text"],
            config["x"],
            config["y"],
            config["width"],
            config["height"],
        )

    def _add_tabs(self, tab: widget.Tabs, board: Board) -> widget.Tabs:
        """Returns tab widget with new tabs.

        This function modifies tab widget and adds new tabs.
        The number of tabs is calculated using number of sensors in board
        and number of possible sensors widgets per single tab.
        Then it adds lables that represents title of columns with sensor widgets.
        """
        sensors = len(board.sensors)
        tabs_num = math.ceil(sensors / self.sensor_config["sensors_per_tab"])
        for t in range(1, tabs_num + 1):
            name = (
                "Sensors "
                + str(1 + (t - 1) * self.sensor_config["sensors_per_tab"])
                + "-"
                + str(self.sensor_config["sensors_per_tab"] * t)
            )
            tab.tab(name)
            value_label = Opi._add_label(self.sensor_config["tab"]["value_label1"])
            value_label.horizontal_alignment_center()
            value_label.vertical_alignment_middle()
            tab.add_widget(name, value_label)

            type_label = Opi._add_label(self.sensor_config["tab"]["type_label1"])
            type_label.horizontal_alignment_center()
            type_label.vertical_alignment_middle()
            tab.add_widget(name, type_label)
            if t < tabs_num:
                value_label = Opi._add_label(self.sensor_config["tab"]["value_label2"])
                value_label.horizontal_alignment_center()
                value_label.vertical_alignment_middle()
                tab.add_widget(name, value_label)

                type_label = Opi._add_label(self.sensor_config["tab"]["type_label2"])
                type_label.horizontal_alignment_center()
                type_label.vertical_alignment_middle()
                tab.add_widget(name, type_label)
            elif t == tabs_num:
                cur_sensors = sensors - self.sensor_config["sensors_per_tab"] * (t - 1)
                if cur_sensors > self.sensor_config["sensors_per_tab"] / 2:
                    value_label = Opi._add_label(
                        self.sensor_config["tab"]["value_label2"]
                    )
                    value_label.horizontal_alignment_center()
                    value_label.vertical_alignment_middle()
                    tab.add_widget(name, value_label)

                    type_label = Opi._add_label(
                        self.sensor_config["tab"]["type_label2"]
                    )
                    type_label.horizontal_alignment_center()
                    type_label.vertical_alignment_middle()
                    tab.add_widget(name, type_label)
        return tab

    def _add_sensor_fields(self, tab: widget.Tabs, board: Board) -> None:
        """Add to tab widget sensor fields.

        This function modifies tab widget and adds set of elements
        describing sensors: text updates with values, type, button to
        open sensor thresholds, labels.
        The current tab, where elements should be added is calculated
        using number of sensors from board and
        and number of possible sensors widgets per single tab.
        Button with sensor thresholds is enabled based on the MTCA.4
        sensor threshold access parameter.
        """
        tab = self._add_tabs(tab, board)
        for i, s in enumerate(board.sensors):
            name = (
                "Sensors "
                + str(
                    1
                    + math.floor(i / self.sensor_config["sensors_per_tab"])
                    * self.sensor_config["sensors_per_tab"]
                )
                + "-"
                + str(
                    self.sensor_config["sensors_per_tab"]
                    + math.floor(i / self.sensor_config["sensors_per_tab"])
                    * self.sensor_config["sensors_per_tab"]
                )
            )
            if i >= self.sensor_config["sensors_per_tab"]:
                i -= (
                    math.floor(i / self.sensor_config["sensors_per_tab"])
                    * self.sensor_config["sensors_per_tab"]
                )
            if i - self.sensor_config["sensors_per_tab"] / 2 < 0:
                x = self.sensor_config["tab"]["x1"]
            else:
                i -= self.sensor_config["sensors_per_tab"] / 2
                x = self.sensor_config["tab"]["x2"]
            label = widget.Label(
                self.sensor_config["tab"]["sensor_label"]["name"] + str(i),
                s.name,
                x,
                self.sensor_config["tab"]["sensor_label"]["y"]
                + i * self.sensor_config["tab"]["sensor_label"]["distance"],
                self.sensor_config["tab"]["sensor_label"]["width"],
                self.sensor_config["tab"]["sensor_label"]["height"],
            )
            label.horizontal_alignment_right()
            label.vertical_alignment_middle()
            tab.add_widget(name, label)
            value = widget.TextUpdate(
                self.sensor_config["tab"]["value_update"]["name"] + str(i),
                self.sensor_config["tab"]["value_update"]["pv1"]
                + s.name
                + self.sensor_config["tab"]["value_update"]["pv2"],
                x + self.sensor_config["tab"]["value_update"]["offset"],
                self.sensor_config["tab"]["value_update"]["y"]
                + i * self.sensor_config["tab"]["value_update"]["distance"],
                self.sensor_config["tab"]["value_update"]["width"],
                self.sensor_config["tab"]["value_update"]["height"],
            )
            value.horizontal_alignment_center()
            value.vertical_alignment_middle()
            tab.add_widget(name, value)

            typ = widget.TextUpdate(
                self.sensor_config["tab"]["type_update"]["name"] + str(i),
                self.sensor_config["tab"]["type_update"]["pv1"]
                + s.name
                + self.sensor_config["tab"]["type_update"]["pv2"],
                x + self.sensor_config["tab"]["type_update"]["offset"],
                self.sensor_config["tab"]["type_update"]["y"]
                + i * self.sensor_config["tab"]["type_update"]["distance"],
                self.sensor_config["tab"]["type_update"]["width"],
                self.sensor_config["tab"]["type_update"]["height"],
            )
            typ.horizontal_alignment_center()
            typ.vertical_alignment_middle()
            tab.add_widget(name, typ)

            macros = {"SENS": s.name}
            details_button = widget.ActionButton(
                self.sensor_config["tab"]["details_button"]["name"] + str(i),
                self.sensor_config["tab"]["details_button"]["text"],
                "",
                x + self.sensor_config["tab"]["details_button"]["offset"],
                self.sensor_config["tab"]["details_button"]["y"]
                + i * self.sensor_config["tab"]["details_button"]["distance"],
                self.sensor_config["tab"]["details_button"]["width"],
                self.sensor_config["tab"]["details_button"]["height"],
            )
            details_button.action_open_display(
                self.sensor_config["tab"]["details_button"]["window"],
                self.sensor_config["tab"]["details_button"]["open"],
                "",
                macros,
            )
            # sensor threshold access: 0-no thresholds 3-fixed, unreadable thresholds
            if s.thresh_access == 0 or s.thresh_access == 3:
                details_button.enabled(False)
            tab.add_widget(name, details_button)

    def _add_sensor_static_elements(self, screen: screen.Screen, board: Board) -> None:
        """Add widgets to screen window.

        This functions add widgets that do not depend on the number of sensors in board.
        Those are widgets informing about board name, fruid and slot that sensors come from.
        They are places in the separate group, below tab widget.
        The functions places also widgets above the tab widget like title, title bar and subtitle.
        It also inserts and configures group for tab widget.
        """
        screen.height(self.sensor_config["screen_height"])
        screen.width(self.sensor_config["screen_width"])

        widgets = []

        # above sensor tab
        rectangle_up = Opi._add_rect(self.sensor_config["bar"])
        rectangle_up.background_color(
            self.colors["bar"]["r"], self.colors["bar"]["g"], self.colors["bar"]["b"]
        )
        rectangle_up.line_width(self.sensor_config["bar"]["line"])
        widgets.append(rectangle_up)

        title = Opi._add_label(self.sensor_config["title"])
        title.predefined_font(fonts.Header1)
        widgets.append(title)

        subtitle = widget.Label(
            self.sensor_config["subtitle"]["name"],
            board.name,
            self.sensor_config["subtitle"]["x"],
            self.sensor_config["subtitle"]["y"],
            self.sensor_config["subtitle"]["width"],
            self.sensor_config["subtitle"]["height"],
        )
        subtitle.predefined_font(fonts.Header2)
        subtitle.horizontal_alignment_right()
        subtitle.vertical_alignment_bottom()
        widgets.append(subtitle)

        screen.add_widget(widgets)

        group = Opi._add_group(self.sensor_config["group"])
        self._add_group_color(group)
        group.transparent(True)
        group.no_style()

        rect = Opi._add_rect(self.sensor_config["group"]["rect"])
        self._add_rect_color(rect)
        rect.corner_height(self.sensor_config["group"]["rect"]["corner_h"])
        rect.corner_width(self.sensor_config["group"]["rect"]["corner_w"])
        self._add_line_color(rect)
        rect.line_width(self.sensor_config["group"]["rect"]["line_w"])
        group.add_widget(rect)

        title = Opi._add_label(self.sensor_config["group"]["title"])
        title.predefined_font(fonts.Header3)
        title.vertical_alignment_middle()
        title.horizontal_alignment_center()
        group.add_widget(title)

        # sensor tab
        tab = widget.Tabs(
            self.sensor_config["tab"]["name"],
            self.sensor_config["tab"]["x"],
            self.sensor_config["tab"]["y"],
            self.sensor_config["tab"]["width"],
            self.sensor_config["tab"]["height"],
        )
        self._add_sensor_fields(tab, board)
        group.add_widget(tab)
        screen.add_widget(group)

        # below sensor tab
        y = (
            self.sensor_config["tab"]["height"]
            + self.sensor_config["details"]["y_offset"]
        )
        board_group = Opi._add_group(self.sensor_config["details"], y=y)
        self._add_group_color(board_group)
        board_group.transparent(True)
        board_group.no_style()

        board_rect = Opi._add_rect(self.sensor_config["details"]["rect"])
        self._add_rect_color(board_rect)
        board_rect.corner_height(self.sensor_config["details"]["rect"]["corner_h"])
        board_rect.corner_width(self.sensor_config["details"]["rect"]["corner_h"])
        self._add_line_color(board_rect)
        board_rect.line_width(self.sensor_config["details"]["rect"]["line_w"])
        board_group.add_widget(board_rect)

        board_title = Opi._add_label(self.sensor_config["details"]["title"])
        board_title.predefined_font(fonts.Header3)
        board_title.vertical_alignment_middle()
        board_title.horizontal_alignment_center()
        board_group.add_widget(board_title)
        for key, item in self.sensor_config["details"].items():
            if "_label" in key:
                label = Opi._add_label(item)
                label.horizontal_alignment_right()
                label.vertical_alignment_middle()
                board_group.add_widget(label)
            elif "_update" in key:
                update = Opi._add_update(item)
                update.horizontal_alignment_center()
                update.vertical_alignment_middle()
                board_group.add_widget(update)
        screen.add_widget(board_group)

    def generate_board_opi(self) -> None:
        """Creates board .bob file.

        This function creates the OPI with available board
        in MTCA.4 chassis.
        """
        self._add_board_static_up_elements()
        group_end_y = self._add_board_dynamic_elements()
        self._add_board_static_down_elements(group_end_y)
        self.board_screen.write_screen(self.save_path / self.opi_name)

    def _add_board_static_up_elements(self) -> None:
        """Adds widgets to board window.

        This function add widget that do not depend on
        number of available boards. Those elements are places
        above the group with boards widgets.
        It adds, title, subtitle, title bar, LED with connection
        status, text update with e3-ipmimanager module version and labels.
        """
        self._add_group_color(self.board_screen)
        self.board_screen.width(self.board_config["screen_width"])

        widgets = []

        rectangle_up = Opi._add_rect(self.board_config["bar"])
        rectangle_up.background_color(
            self.colors["bar"]["r"], self.colors["bar"]["g"], self.colors["bar"]["b"]
        )
        rectangle_up.line_width(self.board_config["bar"]["line"])
        widgets.append(rectangle_up)

        title = Opi._add_label(self.board_config["title"])
        title.predefined_font(fonts.Header1)
        widgets.append(title)

        subtitle = Opi._add_label(self.board_config["subtitle"])
        subtitle.predefined_font(fonts.Header2)
        subtitle.horizontal_alignment_right()
        subtitle.vertical_alignment_bottom()
        widgets.append(subtitle)

        status_label = Opi._add_label(self.board_config["status_label"])
        status_label.predefined_font(fonts.Header3)
        status_label.vertical_alignment_middle()
        status_label.horizontal_alignment_right()
        widgets.append(status_label)

        status_led = widget.LED(
            self.board_config["status_led"]["name"],
            self.board_config["status_led"]["pv"],
            self.board_config["status_led"]["x"],
            self.board_config["status_led"]["y"],
            self.board_config["status_led"]["width"],
            self.board_config["status_led"]["height"],
        )
        status_led.labels_from_pv(False)
        status_led.predefined_off_color(colors.MAJOR)
        status_led.predefined_on_color(colors.OK)
        widgets.append(status_led)

        ipmi_label = Opi._add_label(self.board_config["ipmi_label"])
        ipmi_label.predefined_font(fonts.Header3)
        ipmi_label.vertical_alignment_middle()
        ipmi_label.horizontal_alignment_right()
        widgets.append(ipmi_label)

        ipmi_update = Opi._add_update(self.board_config["ipmi_update"])
        ipmi_update.horizontal_alignment_center()
        ipmi_update.vertical_alignment_middle()
        widgets.append(ipmi_update)

        self.board_screen.add_widget(widgets)

    def _add_board_static_down_elements(self, group_end_y: int) -> None:
        """Adds widgets to board window.

        This functions adds widgets that does not depend on number of available
        boards. They are placed below the group with board widgets.
        Those are: group with interval settings, group with archiver, expert
        button, group with MCH version.
        """
        widgets = []
        y_group = group_end_y + self.board_config["distance"]
        interval_group = Opi._add_group(self.board_config["interval_group"], y=y_group)
        self._add_group_color(interval_group)
        interval_group.transparent(True)
        interval_group.no_style()

        interval_rect = Opi._add_rect(self.board_config["interval_group"]["rect"])
        self._add_rect_color(interval_rect)
        interval_rect.corner_height(
            self.board_config["interval_group"]["rect"]["corner_h"]
        )
        interval_rect.corner_width(
            self.board_config["interval_group"]["rect"]["corner_w"]
        )
        self._add_line_color(interval_rect)
        interval_rect.line_width(self.board_config["interval_group"]["rect"]["line_w"])
        interval_group.add_widget(interval_rect)

        interval_title = Opi._add_label(self.board_config["interval_group"]["title"])
        interval_title.predefined_font(fonts.Header3)
        interval_title.vertical_alignment_middle()
        interval_title.horizontal_alignment_center()
        interval_group.add_widget(interval_title)

        sensors_label = Opi._add_label(
            self.board_config["interval_group"]["sensor_label"]
        )
        sensors_label.horizontal_alignment_right()
        sensors_label.vertical_alignment_middle()
        interval_group.add_widget(sensors_label)

        sensor_spinner = Opi._add_spinner(
            self.board_config["interval_group"]["sensor_spinner"]
        )
        sensor_spinner.format("Decimal")
        sensor_spinner.show_units(True)
        interval_group.add_widget(sensor_spinner)

        thresholds_label = Opi._add_label(
            self.board_config["interval_group"]["thresholds_label"]
        )
        thresholds_label.horizontal_alignment_right()
        thresholds_label.vertical_alignment_middle()
        interval_group.add_widget(thresholds_label)

        threshold_spinner = Opi._add_spinner(
            self.board_config["interval_group"]["thresholds_spinner"]
        )
        threshold_spinner.format("Decimal")
        threshold_spinner.show_units(True)
        interval_group.add_widget(threshold_spinner)

        events_label = Opi._add_label(
            self.board_config["interval_group"]["events_label"]
        )
        events_label.horizontal_alignment_right()
        events_label.vertical_alignment_middle()
        interval_group.add_widget(events_label)

        event_spinner = Opi._add_spinner(
            self.board_config["interval_group"]["event_spinner"]
        )
        event_spinner.format(
            self.board_config["interval_group"]["event_spinner"]["format"]
        )
        event_spinner.show_units(True)
        interval_group.add_widget(event_spinner)

        widgets.append(interval_group)

        archiver_group = Opi._add_group(self.board_config["archiver_group"], y=y_group)
        self._add_group_color(archiver_group)
        archiver_group.transparent(True)
        archiver_group.no_style()

        archiver_rect = Opi._add_rect(self.board_config["archiver_group"]["rect"])
        self._add_rect_color(archiver_rect)
        archiver_rect.corner_height(
            self.board_config["archiver_group"]["rect"]["corner_h"]
        )
        archiver_rect.corner_width(
            self.board_config["archiver_group"]["rect"]["corner_w"]
        )
        self._add_line_color(archiver_rect)
        archiver_rect.line_width(self.board_config["archiver_group"]["rect"]["line_w"])
        archiver_group.add_widget(archiver_rect)

        archiver_title = Opi._add_label(self.board_config["archiver_group"]["title"])
        archiver_title.predefined_font(fonts.Header3)
        archiver_title.vertical_alignment_middle()
        archiver_title.horizontal_alignment_center()
        archiver_group.add_widget(archiver_title)

        archiver_button = widget.BooleanButton(
            self.board_config["archiver_group"]["button"]["name"],
            self.board_config["archiver_group"]["button"]["pv"],
            self.board_config["archiver_group"]["button"]["x"],
            self.board_config["archiver_group"]["button"]["y"],
            self.board_config["archiver_group"]["button"]["width"],
            self.board_config["archiver_group"]["button"]["height"],
        )
        archiver_button.off_color(
            self.colors["button"]["on"]["r"],
            self.colors["button"]["on"]["g"],
            self.colors["button"]["on"]["b"],
        )
        archiver_button.off_label(
            self.board_config["archiver_group"]["button"]["off_label"]
        )
        archiver_button.on_color(
            self.colors["button"]["off"]["r"],
            self.colors["button"]["off"]["g"],
            self.colors["button"]["off"]["b"],
        )
        archiver_button.on_label(
            self.board_config["archiver_group"]["button"]["on_label"]
        )
        archiver_button.show_led(True)
        archiver_group.add_widget(archiver_button)
        widgets.append(archiver_group)

        version_group = Opi._add_group(self.board_config["version_group"], y=y_group)
        self._add_group_color(version_group)
        version_group.transparent(True)
        version_group.no_style()

        version_rect = Opi._add_rect(self.board_config["version_group"]["rect"])
        self._add_rect_color(version_rect)
        version_rect.corner_height(
            self.board_config["version_group"]["rect"]["corner_h"]
        )
        version_rect.corner_width(
            self.board_config["version_group"]["rect"]["corner_w"]
        )
        self._add_line_color(version_rect)
        version_rect.line_width(self.board_config["version_group"]["rect"]["line_w"])
        version_group.add_widget(version_rect)

        version_title = Opi._add_label(self.board_config["version_group"]["title"])
        version_title.predefined_font(fonts.Header3)
        version_title.vertical_alignment_middle()
        version_title.horizontal_alignment_center()
        version_group.add_widget(version_title)

        version_button = widget.ActionButton(
            self.board_config["version_group"]["button"]["name"],
            self.board_config["version_group"]["button"]["action"],
            self.board_config["version_group"]["button"]["pv"],
            self.board_config["version_group"]["button"]["x"],
            self.board_config["version_group"]["button"]["y"],
            self.board_config["version_group"]["button"]["width"],
            self.board_config["version_group"]["button"]["height"],
        )
        version_button.action_write_pv(
            self.board_config["version_group"]["button"]["pv"],
            self.board_config["version_group"]["button"]["value"],
        )
        version_group.add_widget(version_button)

        version_update = Opi._add_update(self.board_config["version_group"]["update"])
        version_update.horizontal_alignment_center()
        version_update.vertical_alignment_middle()
        version_group.add_widget(version_update)
        widgets.append(version_group)

        expert_button = widget.ActionButton(
            self.board_config["expert_button"]["name"],
            self.board_config["expert_button"]["text"],
            "",
            self.board_config["expert_button"]["x"],
            group_end_y + self.board_config["distance"],
            self.board_config["expert_button"]["width"],
            self.board_config["expert_button"]["height"],
        )
        expert_button.action_open_display(
            self.board_config["expert_button"]["window"],
            self.board_config["expert_button"]["open"],
            "",
        )

        widgets.append(expert_button)
        self.board_screen.add_widget(widgets)

    def _add_board_dynamic_elements(self) -> int:
        """Add widgets to board window.

        This functions add widgets associated with number of
        available boards. For each board there are slot, name,
        fru text updates, LED with M-state and buttons.
        """
        group_widgets = []
        cnt_col1 = -1
        cnt_col2 = -1
        for b in self.boards:
            if b.type_ in self.board_config["modules"]:
                x = self.board_config["board_group"]["x_row2"]
                cnt_col2 += 1
                cnt = cnt_col2
            else:
                x = self.board_config["board_group"]["x_row"]
                cnt_col1 += 1
                cnt = cnt_col1

            module_label = widget.Label(
                self.board_config["board_group"]["module"]["name1"]
                + str(cnt + 1)
                + self.board_config["board_group"]["module"]["name2"],
                self.board_config["board_group"]["module"]["text"],
                x,
                self.board_config["board_group"]["module"]["y"]
                + cnt * self.board_config["board_group"]["module"]["distance"],
                self.board_config["board_group"]["module"]["width"],
                self.board_config["board_group"]["module"]["height"],
            )
            module_label.vertical_alignment_middle()
            module_label.horizontal_alignment_right()
            group_widgets.append(module_label)

            slot = widget.TextUpdate(
                self.board_config["board_group"]["slot"]["name1"]
                + str(cnt + 1)
                + self.board_config["board_group"]["slot"]["name2"],
                self.board_config["board_group"]["slot"]["pv1"]
                + str(b.slot)
                + self.board_config["board_group"]["slot"]["pv2"],
                x + self.board_config["board_group"]["slot"]["offset"],
                self.board_config["board_group"]["slot"]["y"]
                + cnt * self.board_config["board_group"]["slot"]["distance"],
                self.board_config["board_group"]["slot"]["width"],
                self.board_config["board_group"]["slot"]["height"],
            )
            slot.horizontal_alignment_center()
            slot.vertical_alignment_middle()
            group_widgets.append(slot)

            name = widget.TextUpdate(
                self.board_config["board_group"]["name"]["name1"]
                + str(cnt + 1)
                + self.board_config["board_group"]["name"]["name2"],
                self.board_config["board_group"]["name"]["pv1"]
                + str(b.slot)
                + self.board_config["board_group"]["name"]["pv2"],
                x + self.board_config["board_group"]["name"]["offset"],
                self.board_config["board_group"]["name"]["y"]
                + cnt * self.board_config["board_group"]["name"]["distance"],
                self.board_config["board_group"]["name"]["width"],
                self.board_config["board_group"]["name"]["height"],
            )
            name.horizontal_alignment_center()
            name.vertical_alignment_middle()
            group_widgets.append(name)

            fru = widget.TextUpdate(
                self.board_config["board_group"]["fru"]["name1"]
                + str(cnt + 1)
                + self.board_config["board_group"]["fru"]["name2"],
                self.board_config["board_group"]["fru"]["pv1"]
                + str(b.slot)
                + self.board_config["board_group"]["fru"]["pv2"],
                x + self.board_config["board_group"]["fru"]["offset"],
                self.board_config["board_group"]["fru"]["y"]
                + cnt * self.board_config["board_group"]["fru"]["distance"],
                self.board_config["board_group"]["fru"]["width"],
                self.board_config["board_group"]["fru"]["height"],
            )
            fru.horizontal_alignment_center()
            fru.vertical_alignment_middle()
            group_widgets.append(fru)

            led_multi = widget.LEDMultiState(
                self.board_config["board_group"]["led"]["name1"]
                + str(cnt + 1)
                + self.board_config["board_group"]["led"]["name2"],
                self.board_config["board_group"]["led"]["pv1"]
                + str(b.slot)
                + self.board_config["board_group"]["led"]["pv2"],
                x + self.board_config["board_group"]["led"]["offset"],
                self.board_config["board_group"]["led"]["y"]
                + cnt * self.board_config["board_group"]["led"]["distance"],
                self.board_config["board_group"]["led"]["width"],
                self.board_config["board_group"]["led"]["height"],
            )
            led_multi.square(True)
            for i in range(0, 8):
                idx = str(i)
                led_multi.state(
                    i,
                    self.board_config["board_group"]["led"][idx],
                    self.colors["led"]["M" + idx]["r"],
                    self.colors["led"]["M" + idx]["g"],
                    self.colors["led"]["M" + idx]["b"],
                )
            led_multi.fallback_color(
                self.colors["led"]["fallback"]["r"],
                self.colors["led"]["fallback"]["g"],
                self.colors["led"]["fallback"]["b"],
            )
            led_multi.fallback_label(self.board_config["board_group"]["led"]["err"])
            group_widgets.append(led_multi)

            sensors_button = widget.ActionButton(
                self.board_config["board_group"]["sensor_button"]["name1"]
                + str(cnt + 1)
                + self.board_config["board_group"]["sensor_button"]["name2"],
                self.board_config["board_group"]["sensor_button"]["text"],
                "",
                x + self.board_config["board_group"]["sensor_button"]["offset"],
                self.board_config["board_group"]["sensor_button"]["y"]
                + cnt * self.board_config["board_group"]["sensor_button"]["distance"],
                self.board_config["board_group"]["sensor_button"]["width"],
                self.board_config["board_group"]["sensor_button"]["height"],
            )
            macros = {"R": "$(R_SLOT" + str(b.slot) + ")"}
            sensors_button.action_open_display(
                f"sensors/{b.file_name}_slot{b.slot}.bob",
                self.board_config["board_group"]["sensor_button"]["open"],
                self.board_config["board_group"]["sensor_button"]["desc"],
                macros,
            )
            group_widgets.append(sensors_button)

            more_button = widget.ActionButton(
                self.board_config["board_group"]["more_button"]["name1"]
                + str(cnt + 1)
                + self.board_config["board_group"]["more_button"]["name2"],
                self.board_config["board_group"]["more_button"]["text"],
                "",
                x + self.board_config["board_group"]["more_button"]["offset"],
                self.board_config["board_group"]["more_button"]["y"]
                + cnt * self.board_config["board_group"]["more_button"]["distance"],
                self.board_config["board_group"]["more_button"]["width"],
                self.board_config["board_group"]["more_button"]["height"],
            )
            more_button.action_open_display(
                "fru/DetailsFRU.bob", "window", "FRU", macros
            )
            more_button.action_open_display(
                "expert/ExpertFRU.bob", "window", "Expert", macros
            )
            group_widgets.append(more_button)

        width = cnt_col1
        if cnt_col1 < cnt_col2:
            width = cnt_col2
        width += 1
        h = (
            width * self.board_config["board_group"]["group"]["distance"]
            + self.board_config["board_group"]["group"]["offset"]
        )
        group = Opi._add_group(self.board_config["board_group"]["group"], height=h)
        self._add_group_color(group)
        group.transparent(True)
        group.no_style()

        rectangle_group = widget.Rectangle(
            self.board_config["board_group"]["rect"]["name"],
            self.board_config["board_group"]["rect"]["x"],
            self.board_config["board_group"]["rect"]["y"],
            self.board_config["board_group"]["rect"]["width"],
            width * self.board_config["board_group"]["rect"]["distance"]
            + self.board_config["board_group"]["rect"]["offset"],
        )
        self._add_rect_color(rectangle_group)
        rectangle_group.corner_height(
            self.board_config["board_group"]["rect"]["corner_h"]
        )
        rectangle_group.corner_width(
            self.board_config["board_group"]["rect"]["corner_w"]
        )
        self._add_line_color(rectangle_group)
        rectangle_group.line_width(self.board_config["board_group"]["rect"]["line_w"])

        group_title = Opi._add_label(self.board_config["board_group"]["title"])
        group_title.predefined_font(fonts.Header2)
        group_title.vertical_alignment_middle()
        group_title.horizontal_alignment_center()
        group.add_widget([rectangle_group, group_title])

        slot_label = Opi._add_label(self.board_config["board_group"]["slot_label"])
        slot_label.horizontal_alignment_center()
        slot_label.vertical_alignment_middle()
        name_label = Opi._add_label(self.board_config["board_group"]["name_label"])
        name_label.horizontal_alignment_center()
        name_label.vertical_alignment_middle()
        fru_label = Opi._add_label(self.board_config["board_group"]["fru_label"])
        fru_label.horizontal_alignment_center()
        fru_label.vertical_alignment_middle()
        slot_label_2 = Opi._add_label(self.board_config["board_group"]["slot_label2"])
        slot_label_2.horizontal_alignment_center()
        slot_label_2.vertical_alignment_middle()
        name_label_2 = Opi._add_label(self.board_config["board_group"]["name_label2"])
        name_label_2.horizontal_alignment_center()
        name_label_2.vertical_alignment_middle()
        fru_label_2 = Opi._add_label(self.board_config["board_group"]["fru_label2"])
        fru_label_2.horizontal_alignment_center()
        fru_label_2.vertical_alignment_middle()
        group_widgets.append(slot_label)
        group_widgets.append(slot_label_2)
        group_widgets.append(name_label)
        group_widgets.append(name_label_2)
        group_widgets.append(fru_label)
        group_widgets.append(fru_label_2)

        group.add_widget(group_widgets)
        self.board_screen.add_widget(group)
        return (
            width * self.board_config["board_group"]["distance"]
            + self.board_config["board_group"]["offset"]
        )
