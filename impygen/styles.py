# -*- coding: utf-8 -*-

"""Classes representing enum values for naming style and macros convention."""

from enum import Flag


class NamingStyle(Flag):
    """Represents type of sensor naming style.

    CONTROLLERS_AS_DEVICES - use sensor names in PV files
    CONTROLLERS_AS_SUBSYSTEMS - use sensor numbers in PV files
    """

    CONTROLLERS_AS_DEVICES = False
    CONTROLLERS_AS_SUBSYSTEMS = True

    def __str__(self):
        return self.name.lower()

    @staticmethod
    def from_string(s):
        try:
            return NamingStyle[s.upper()]
        except KeyError:
            raise ValueError()


class MacrosStyle(Flag):
    """Represennts prefix macros convention.

    SEPARATED_PREFIX - use standard macros $(P)$(R)Property
    SINGLE_PREFIX - use macros $(P)$(BOARD_TYPE)-$(ID)Property
    """

    SEPARATED_PREFIX = False
    SINGLE_PREFIX = True

    def __str__(self):
        return self.name.lower()

    @staticmethod
    def from_string(s):
        try:
            return MacrosStyle[s.upper()]
        except KeyError:
            raise ValueError()
