# PVs for ${SENSOR_NAME} sensors from board
record(longin, "$(P)$(R)${SENSOR_NAME}EvtrdType"){
    field(DTYP, "asynInt32")
    field(INP, "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s${SINDEX}.EvtrdType")
    field(SCAN, "I/O Intr")
    field(DESC, "${SENSOR_NAME} evt rd type")
    info(DESCRIPTION, "${BOARD_NAME} ${SENSOR_NAME} event rd type")
}

record(longin, "$(P)$(R)${SENSOR_NAME}SdrType"){
    field(DTYP, "asynInt32")
    field(INP, "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s${SINDEX}.SdrType")
    field(SCAN, "I/O Intr")
    field(DESC, "${SENSOR_NAME} sdr type")
    info(DESCRIPTION, "${BOARD_NAME} ${SENSOR_NAME} sdr type")
}

record(stringin, "$(P)$(R)${SENSOR_NAME}Name"){
    field(DTYP, "asynOctetRead")
    field(INP, "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s${SINDEX}.Name")
    field(SCAN, "I/O Intr")
    field(DESC, "${SENSOR_NAME} name")
    info(DESCRIPTION, "${BOARD_NAME} ${SENSOR_NAME} name")
}

record(stringin, "$(P)$(R)${SENSOR_NAME}Type"){
    field(DTYP, "asynOctetRead")
    field(INP, "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s${SINDEX}.Type")
    field(SCAN, "I/O Intr")
    field(DESC, "${SENSOR_NAME} type")
    info(DESCRIPTION, "${BOARD_NAME} ${SENSOR_NAME} sensor type")
}

record(stringin, "$(P)$(R)${SENSOR_NAME}Units"){
    field(DTYP, "asynOctetRead")
    field(INP, "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s${SINDEX}.Units")
    field(SCAN, "I/O Intr")
    field(DESC, "${SENSOR_NAME} units")
    info(DESCRIPTION, "${BOARD_NAME} ${SENSOR_NAME} sensor units")
}

record(ai, "$(P)$(R)${SENSOR_NAME}NormMax"){
    field(DTYP, "asynFloat64")
    field(INP, "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s${SINDEX}.NormMax")
    field(SCAN, "I/O Intr")
    field(DESC, "${SENSOR_NAME} norm max")
    info(DESCRIPTION, "${BOARD_NAME} ${SENSOR_NAME} normal maximum")
    field(EGU, "${UNIT}")
}

record(ai, "$(P)$(R)${SENSOR_NAME}NormMin"){
    field(DTYP, "asynFloat64")
    field(INP, "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s${SINDEX}.NormMin")
    field(SCAN, "I/O Intr")
    field(DESC, "${SENSOR_NAME} norm min")
    info(DESCRIPTION, "${BOARD_NAME} ${SENSOR_NAME} normal minimum")
    field(EGU, "${UNIT}")
}

record(ai, "$(P)$(R)${SENSOR_NAME}HystPos"){
    field(DTYP, "asynFloat64")
    field(INP, "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s${SINDEX}.HystPos")
    field(SCAN, "I/O Intr")
    field(DESC, "${SENSOR_NAME} hyst pos")
    info(DESCRIPTION, "${BOARD_NAME} ${SENSOR_NAME} hystheresis positive")
    field(EGU, "${UNIT}")
}

record(ai, "$(P)$(R)${SENSOR_NAME}HystNeg"){
    field(DTYP, "asynFloat64")
    field(INP, "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s${SINDEX}.HystNeg")
    field(SCAN, "I/O Intr")
    field(DESC, "${SENSOR_NAME} hyst neg")
    info(DESCRIPTION, "${BOARD_NAME} ${SENSOR_NAME} hystheresis negative")
    field(EGU, "${UNIT}")
}

record(ai, "$(P)$(R)${SENSOR_NAME}Value"){
    field(DTYP, "asynFloat64")
    field(INP, "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s${SINDEX}.Value")
    field(SCAN, "I/O Intr")
    field(HHSV, "MAJOR")
    field(HSV, "MINOR")
    field(LLSV, "MAJOR")
    field(LSV, "MINOR")
    field(DESC, "${SENSOR_NAME} value")
    info(DESCRIPTION, "${BOARD_NAME} ${SENSOR_NAME} sensor value")
    field(EGU, "${UNIT}")
}

record(longin, "$(P)$(R)${SENSOR_NAME}ThrUpdtAllow"){
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s${SINDEX}.ThrUpdtAllow")
    field(SCAN, "I/O Intr")
    field(DESC, "${SENSOR_NAME} thr updt allow")
    info(DESCRIPTION, "${BOARD_NAME} ${SENSOR_NAME} if threshold modification is allowed")
}

record(longin, "$(P)$(R)${SENSOR_NAME}ThrAccess"){
    field(DESC, "${SENSOR_NAME} thr access")
    info(DESCRIPTION, "${BOARD_NAME} ${SENSOR_NAME} threshold access mask")
    field(VAL, "${THR_ACCESS}")
    field(SCAN, "Passive")
}

record(waveform, "$(P)$(R)${SENSOR_NAME}Wave"){
    field(DTYP, "asynFloat64ArrayIn")
    field(INP, "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s${SINDEX}.Wave")
    field(NELM, "$(ARCHIVER_SIZE)")
    field(FTVL, "DOUBLE")
    field(SCAN, "I/O Intr")
    field(DESC, "${SENSOR_NAME} arch data")
    info(DESCRIPTION, "${BOARD_NAME} ${SENSOR_NAME} archiver waveform")
    field(EGU, "${UNIT}")
}
################################################################
