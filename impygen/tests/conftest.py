import json
from pathlib import Path

import pytest


@pytest.fixture(scope="module")
def config_dict():
    root = Path(__file__).parent
    config_dict = {
        "test_board1": json.loads(Path(root, "inputs", "test_board1.json").read_text()),
        "test_board2": json.loads(Path(root, "inputs", "test_board2.json").read_text()),
        "test_board3": json.loads(Path(root, "inputs", "test_board3.json").read_text()),
        "test_chassis1": json.loads(
            Path(root, "inputs", "test_chassis1.json").read_text()
        ),
        "test_chassis2": json.loads(
            Path(root, "inputs", "test_chassis2.json").read_text()
        ),
        "test_sensor1": json.loads(
            Path(root, "inputs", "test_sensor1.json").read_text()
        ),
        "test_sensor2": json.loads(
            Path(root, "inputs", "test_sensor2.json").read_text()
        ),
        "test_sensor3": json.loads(
            Path(root, "inputs", "test_sensor3.json").read_text()
        ),
    }
    return config_dict


@pytest.fixture
def db_file(tmp_path):
    db_file = tmp_path / "test.db"
    db_file.touch()
    return db_file
