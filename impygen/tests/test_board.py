import pytest

from impygen.db.board import Board
from impygen.styles import MacrosStyle, NamingStyle
from impygen.tests import vars as vars_


@pytest.mark.parametrize(
    "test_board,expected",
    [
        (
            "test_board1",
            "CU",
        ),
        (
            "test_board2",
            "$(BOARD_TYPE=AMC)",
        ),
        (
            "test_board3",
            "MTCA",
        ),
    ],
)
def test_type_(config_dict, test_board, expected):
    module = Board(
        config_dict[test_board],
        naming_style=NamingStyle.CONTROLLERS_AS_SUBSYSTEMS,
        macros_style=MacrosStyle.SINGLE_PREFIX,
    )
    assert module.type_ == expected


@pytest.mark.parametrize(
    "test_board,expected",
    [
        (
            "test_board1",
            "",
        ),
        (
            "test_board2",
            "",
        ),
        (
            "test_board3",
            "Clk-",
        ),
    ],
)
def test_opt(config_dict, test_board, expected):
    module = Board(
        config_dict[test_board],
        naming_style=NamingStyle.CONTROLLERS_AS_SUBSYSTEMS,
        macros_style=MacrosStyle.SINGLE_PREFIX,
    )
    assert module.opt == expected


@pytest.mark.parametrize(
    "test_board,inp",
    [
        (
            "test_board1",
            vars_.input_macros1,
        ),
        (
            "test_board2",
            vars_.input_macros2,
        ),
        (
            "test_board3",
            vars_.input_macros2,
        ),
    ],
)
def test_replace_macros_in_templates(db_file, config_dict, test_board, inp):
    module = Board(
        config_dict[test_board],
        naming_style=NamingStyle.CONTROLLERS_AS_SUBSYSTEMS,
        macros_style=MacrosStyle.SINGLE_PREFIX,
    )
    db_file.write_text(inp)
    module._replace_macros(db_file_name=db_file)
    text = db_file.read_text()
    assert not any(
        macro in text for macro in ["${BOARD_NAME}", "${OPT}", "${BOARD_TYPE}"]
    )


@pytest.mark.parametrize(
    "test_board,macros,exp_name,expected",
    [
        (
            "test_board1",
            MacrosStyle.SINGLE_PREFIX,
            "SchroffuTCACU_slot48.db",
            vars_.board_exp1,
        ),
        (
            "test_board2",
            MacrosStyle.SEPARATED_PREFIX,
            "CCTAM900_412_slot1.db",
            vars_.board_exp2,
        ),
    ],
)
def test_create_dbs(tmp_path, config_dict, test_board, macros, exp_name, expected):
    module = Board(
        config_dict[test_board],
        naming_style=NamingStyle.CONTROLLERS_AS_SUBSYSTEMS,
        macros_style=macros,
        db_path=tmp_path,
    )
    file_name = module._create_dbs()
    assert file_name == tmp_path / exp_name
    assert len(list(module.db_dir.iterdir())) == 1
    assert expected in file_name.read_text()


@pytest.mark.parametrize(
    "test_board,macros,naming,exp_name,exp_board,exp_sens",
    [
        (
            "test_board1",
            MacrosStyle.SINGLE_PREFIX,
            NamingStyle.CONTROLLERS_AS_DEVICES,
            "SchroffuTCACU_slot48.db",
            vars_.board_exp4,
            vars_.board_sens_exp4,
        ),
        (
            "test_board2",
            MacrosStyle.SEPARATED_PREFIX,
            NamingStyle.CONTROLLERS_AS_SUBSYSTEMS,
            "CCTAM900_412_slot1.db",
            vars_.board_exp3,
            vars_.board_sens_exp3,
        ),
    ],
)
def test_fill_dbs(
    tmp_path, config_dict, test_board, macros, naming, exp_name, exp_board, exp_sens
):
    module = Board(
        config_dict[test_board],
        naming_style=naming,
        macros_style=macros,
        db_path=tmp_path,
    )
    module.fill_dbs()
    file_name = tmp_path / exp_name
    assert file_name == list(tmp_path.glob("*.db"))[0]
    assert len(list(module.db_dir.iterdir())) == 1
    assert exp_board in file_name.read_text()
    assert exp_sens in file_name.read_text()
