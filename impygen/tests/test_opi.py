import os

import pytest

from impygen.opi import Opi


def test_create_dirs(tmp_path, config_dict):
    dirs = ["sensors", "expert", "fru", "thresholds", "archiver"]
    opis = Opi(save_dir=tmp_path, boards=config_dict["test_board1"])
    opis.create_dirs()
    path = tmp_path
    assert path.is_dir()
    assert sorted(dirs) == sorted(os.listdir(path))


def test_copy_images(tmp_path, config_dict):
    imgs = ["Graph_lower.gif", "Graph_upper.gif"]
    opis = Opi(save_dir=tmp_path, boards=config_dict["test_board1"])
    opis._copy_images()
    path = tmp_path / "images"
    assert path.is_dir()
    assert sorted(imgs) == sorted(os.listdir(path))


@pytest.mark.parametrize(
    "folder,bob",
    [
        (
            "expert",
            "ExpertFRU.bob",
        ),
        (
            "expert",
            "ExpertChassis.bob",
        ),
        (
            "fru",
            "DetailsFRU.bob",
        ),
        (
            "archiver",
            "Archiver.bob",
        ),
        (
            "thresholds",
            "SensorThresholds.bob",
        ),
    ],
)
def test_copy_common_opis(tmp_path, config_dict, folder, bob):
    opis = Opi(save_dir=tmp_path, boards=config_dict["test_board1"])
    opis.create_dirs()
    opis.copy_common_opis()
    path = tmp_path / folder / bob
    assert path.is_file()
