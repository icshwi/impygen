from pathlib import Path

import pytest

from impygen.db.sensors import Sensor
from impygen.styles import MacrosStyle, NamingStyle
from impygen.tests import vars as vars_


@pytest.mark.parametrize(
    "test_sensor,naming,macros,expected",
    [
        (
            "test_sensor1",
            NamingStyle.CONTROLLERS_AS_SUBSYSTEMS,
            MacrosStyle.SINGLE_PREFIX,
            Path(__file__).parent.parent
            / "templates/db_templates/not_modifiable_thresholds.template",
        ),
        (
            "test_sensor1",
            NamingStyle.CONTROLLERS_AS_SUBSYSTEMS,
            MacrosStyle.SEPARATED_PREFIX,
            Path(__file__).parent.parent
            / "templates/db_templates/pr_not_modifiable_thresholds.template",
        ),
        (
            "test_sensor1",
            NamingStyle.CONTROLLERS_AS_DEVICES,
            MacrosStyle.SEPARATED_PREFIX,
            Path(__file__).parent.parent
            / "templates/db_templates/pr_not_modifiable_thresholds.template",
        ),
        (
            "test_sensor1",
            NamingStyle.CONTROLLERS_AS_DEVICES,
            MacrosStyle.SINGLE_PREFIX,
            Path(__file__).parent.parent
            / "templates/db_templates/not_modifiable_thresholds.template",
        ),
        (
            "test_sensor2",
            NamingStyle.CONTROLLERS_AS_SUBSYSTEMS,
            MacrosStyle.SINGLE_PREFIX,
            Path(__file__).parent.parent
            / "templates/db_templates/modifiable_thresholds.template",
        ),
        (
            "test_sensor2",
            NamingStyle.CONTROLLERS_AS_SUBSYSTEMS,
            MacrosStyle.SEPARATED_PREFIX,
            Path(__file__).parent.parent
            / "templates/db_templates/pr_modifiable_thresholds.template",
        ),
        (
            "test_sensor2",
            NamingStyle.CONTROLLERS_AS_DEVICES,
            MacrosStyle.SEPARATED_PREFIX,
            Path(__file__).parent.parent
            / "templates/db_templates/pr_modifiable_thresholds.template",
        ),
        (
            "test_sensor2",
            NamingStyle.CONTROLLERS_AS_SUBSYSTEMS,
            MacrosStyle.SINGLE_PREFIX,
            Path(__file__).parent.parent
            / "templates/db_templates/modifiable_thresholds.template",
        ),
    ],
)
def test_get_thresh_template(config_dict, test_sensor, naming, macros, expected):
    sensor = Sensor(config_dict[test_sensor], naming_style=naming, macros_style=macros)
    assert sensor._get_thresh_template() == expected


@pytest.mark.parametrize(
    "test_sensor,thresh,desc,template,expected",
    [
        (
            "test_sensor1",
            "ThrLoNonCrt",
            "threshold low non-critical",
            Path(__file__).parent.parent
            / "templates/db_templates/not_modifiable_thresholds.template",
            vars_.not_mod_thresh_exp,
        ),
        (
            "test_sensor1",
            "ThrLoCrt",
            "threshold low critical",
            Path(__file__).parent.parent
            / "templates/db_templates/pr_not_modifiable_thresholds.template",
            vars_.pr_not_mod_thresh_exp,
        ),
        (
            "test_sensor1",
            "ThrUpNonCrt",
            "threshold upper non-critical",
            Path(__file__).parent.parent
            / "templates/db_templates/modifiable_thresholds.template",
            vars_.mod_thresh_exp,
        ),
        (
            "test_sensor1",
            "ThrUpCrt",
            "threshold upper critical",
            Path(__file__).parent.parent
            / "templates/db_templates/pr_modifiable_thresholds.template",
            vars_.pr_mod_thresh_exp,
        ),
    ],
)
def test_create_thresh_file(
    config_dict, db_file, test_sensor, thresh, desc, template, expected, tmp_path
):
    sensor = Sensor(
        config_dict[test_sensor],
        naming_style=NamingStyle.CONTROLLERS_AS_SUBSYSTEMS,
        macros_style=MacrosStyle.SINGLE_PREFIX,
    )
    sensor._create_thresh_file(
        db_file_name=db_file,
        template_name=template,
        threshold_name=thresh,
        description=desc,
    )
    assert len(list(tmp_path.iterdir())) == 1
    assert db_file.read_text() == expected


@pytest.mark.parametrize(
    "test_sensor,naming,macros,expected",
    [
        (
            "test_sensor1",
            NamingStyle.CONTROLLERS_AS_SUBSYSTEMS,
            MacrosStyle.SINGLE_PREFIX,
            vars_.sensor1_exp_enumer,
        ),
        (
            "test_sensor2",
            NamingStyle.CONTROLLERS_AS_DEVICES,
            MacrosStyle.SEPARATED_PREFIX,
            vars_.sensor2_exp,
        ),
    ],
)
def test_add_sensors(
    config_dict, db_file, test_sensor, naming, macros, expected, tmp_path
):
    sensor = Sensor(config_dict[test_sensor], naming_style=naming, macros_style=macros)
    sensor.add_sensors(db_file_name=db_file)
    assert len(list(tmp_path.iterdir())) == 1
    assert expected in db_file.read_text()


@pytest.mark.parametrize(
    "test_sensor,naming,macros,inp,expected",
    [
        (
            "test_sensor1",
            NamingStyle.CONTROLLERS_AS_SUBSYSTEMS,
            MacrosStyle.SINGLE_PREFIX,
            vars_.macros_input1,
            vars_.macros_exp1,
        ),
        (
            "test_sensor1",
            NamingStyle.CONTROLLERS_AS_DEVICES,
            MacrosStyle.SINGLE_PREFIX,
            vars_.macros_input2,
            vars_.macros_exp2,
        ),
    ],
)
def test_replace_macros(
    config_dict, db_file, test_sensor, naming, macros, inp, expected
):
    db_file.write_text(inp)
    sensor = Sensor(config_dict[test_sensor], naming_style=naming, macros_style=macros)
    sensor.replace_macros(db_file)
    assert db_file.read_text() == expected


@pytest.mark.parametrize(
    "test_sensor,exp_num",
    [
        (
            "test_sensor1",
            1,
        ),
        (
            "test_sensor2",
            1,
        ),
        (
            "test_sensor3",
            0,
        ),
    ],
)
def test_add_thresholds(config_dict, tmp_path, test_sensor, exp_num):
    sensor = Sensor(
        config_dict[test_sensor],
        naming_style=NamingStyle.CONTROLLERS_AS_SUBSYSTEMS,
        macros_style=MacrosStyle.SINGLE_PREFIX,
    )
    sensor.add_thresholds(db_file_name=tmp_path / "test.db")
    assert len(list(tmp_path.iterdir())) == exp_num


@pytest.mark.parametrize(
    "test_sensor",
    [
        "test_sensor1",
        "test_sensor2",
        "test_sensor3",
    ],
)
def test_load_thresh_template(config_dict, db_file, tmp_path, test_sensor):
    sensor = Sensor(
        config_dict[test_sensor],
        naming_style=NamingStyle.CONTROLLERS_AS_SUBSYSTEMS,
        macros_style=MacrosStyle.SINGLE_PREFIX,
    )
    sensor._load_thresh_template(db_name=db_file)
    text = db_file.read_text()
    assert not any(
        macro in text for macro in ["${THRESHOLD_NAME}", "${THRESHOLD_DESC}"]
    )
    assert len(list(tmp_path.iterdir())) == 1
