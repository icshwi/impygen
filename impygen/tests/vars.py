not_mod_thresh_exp = """
record(ai, "$(P)${BOARD_TYPE}-$(ID)${OPT}${SENSOR_NAME}ThrLoNonCrt"){
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s${SINDEX}.ThrLoNonCrt")
    field(SCAN, "I/O Intr")
    field(DESC, "${SENSOR_NAME} thr ThrLoNonCrt")
    field(EGU,  "${UNIT}")
    info(DESCRIPTION, "${BOARD_NAME} ${SENSOR_NAME} threshold low non-critical")
}
"""

pr_not_mod_thresh_exp = """
record(ai, "$(P)$(R)${SENSOR_NAME}ThrLoCrt"){
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s${SINDEX}.ThrLoCrt")
    field(SCAN, "I/O Intr")
    field(DESC, "${SENSOR_NAME} thr ThrLoCrt")
    field(EGU,  "${UNIT}")
    info(DESCRIPTION, "${BOARD_NAME} ${SENSOR_NAME} threshold low critical")
}
"""

mod_thresh_exp = """
record(ai, "$(P)${BOARD_TYPE}-$(ID)${OPT}${SENSOR_NAME}ThrUpNonCrt"){
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s${SINDEX}.ThrUpNonCrt")
    field(SCAN, "I/O Intr")
    field(DESC, "${SENSOR_NAME} ThrUpNonCrt")
    field(EGU,  "${UNIT}")
    info(DESCRIPTION, "${BOARD_NAME} ${SENSOR_NAME} threshold upper non-critical")
}

record(ao, "$(P)${BOARD_TYPE}-$(ID)${OPT}${SENSOR_NAME}ThrUpNonCrt-SP"){
    field(DTYP, "asynFloat64")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s${SINDEX}.ThrUpNonCrt-SP")
    field(DESC, "${SENSOR_NAME} ThrUpNonCrt setpoint")
    field(EGU,  "${UNIT}")
    info(DESCRIPTION, "${BOARD_NAME} ${SENSOR_NAME} threshold upper non-critical setpoint")
}

record(longin, "$(P)${BOARD_TYPE}-$(ID)${OPT}${SENSOR_NAME}ThrUpNonCrtStat"){
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s${SINDEX}.ThrUpNonCrtStat")
    field(SCAN, "I/O Intr")
    field(DESC, "${SENSOR_NAME} ThrUpNonCrt state")
    info(DESCRIPTION, "${BOARD_NAME} ${SENSOR_NAME} threshold upper non-critical state")
}
"""

pr_mod_thresh_exp = """
record(ai, "$(P)$(R)${SENSOR_NAME}ThrUpCrt"){
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s${SINDEX}.ThrUpCrt")
    field(SCAN, "I/O Intr")
    field(DESC, "${SENSOR_NAME} ThrUpCrt")
    field(EGU,  "${UNIT}")
    info(DESCRIPTION, "${BOARD_NAME} ${SENSOR_NAME} threshold upper critical")
}

record(ao, "$(P)$(R)${SENSOR_NAME}ThrUpCrt-SP"){
    field(DTYP, "asynFloat64")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s${SINDEX}.ThrUpCrt-SP")
    field(DESC, "${SENSOR_NAME} ThrUpCrt setpoint")
    field(EGU,  "${UNIT}")
    info(DESCRIPTION, "${BOARD_NAME} ${SENSOR_NAME} threshold upper critical setpoint")
}

record(longin, "$(P)$(R)${SENSOR_NAME}ThrUpCrtStat"){
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s${SINDEX}.ThrUpCrtStat")
    field(SCAN, "I/O Intr")
    field(DESC, "${SENSOR_NAME} ThrUpCrt state")
    info(DESCRIPTION, "${BOARD_NAME} ${SENSOR_NAME} threshold upper critical state")
}
"""

sensor1_exp_enumer = """# PVs for Sensor4 sensors from board
record(longin, "$(P)${BOARD_TYPE}-$(ID)${OPT}Sensor4EvtrdType"){
    field(DTYP, "asynInt32")
    field(INP, "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s4.EvtrdType")
    field(SCAN, "I/O Intr")
    field(DESC, "Sensor4 evt rd type")
    info(DESCRIPTION, "${BOARD_NAME} Sensor4 event rd type")
}

record(longin, "$(P)${BOARD_TYPE}-$(ID)${OPT}Sensor4SdrType"){
    field(DTYP, "asynInt32")
    field(INP, "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s4.SdrType")
    field(SCAN, "I/O Intr")
    field(DESC, "Sensor4 sdr type")
    info(DESCRIPTION, "${BOARD_NAME} Sensor4 sdr type")
}
"""

sensor2_exp = """# PVs for TCOOLERUPM sensors from board
record(longin, "$(P)$(R)TCOOLERUPMEvtrdType"){
    field(DTYP, "asynInt32")
    field(INP, "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s25.EvtrdType")
    field(SCAN, "I/O Intr")
    field(DESC, "TCOOLERUPM evt rd type")
    info(DESCRIPTION, "${BOARD_NAME} TCOOLERUPM event rd type")
}

record(longin, "$(P)$(R)TCOOLERUPMSdrType"){
    field(DTYP, "asynInt32")
    field(INP, "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s25.SdrType")
    field(SCAN, "I/O Intr")
    field(DESC, "TCOOLERUPM sdr type")
    info(DESCRIPTION, "${BOARD_NAME} TCOOLERUPM sdr type")
}
"""

macros_input1 = """
record(waveform, "$(P)$(R)${SENSOR_NAME}Wave"){
    field(DTYP, "asynFloat64ArrayIn")
    field(INP, "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s${SINDEX}.Wave")
    field(NELM, "$(ARCHIVER_SIZE)")
    field(FTVL, "DOUBLE")
    field(SCAN, "I/O Intr")
    field(DESC, "${SENSOR_NAME} arch data")
    info(DESCRIPTION, "${BOARD_NAME} ${SENSOR_NAME} archiver waveform")
    field(EGU, "${UNIT}")
}
"""

macros_input2 = """
record(waveform, "$(P)${BOARD_TYPE}-$(ID)${OPT}${SENSOR_NAME}Wave"){
    field(DTYP, "asynFloat64ArrayIn")
    field(INP, "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s${SINDEX}.Wave")
    field(NELM, "$(ARCHIVER_SIZE)")
    field(FTVL, "DOUBLE")
    field(SCAN, "I/O Intr")
    field(DESC, "${SENSOR_NAME} arch data")
    info(DESCRIPTION, "${BOARD_NAME} ${SENSOR_NAME} archiver waveform")
    field(EGU, "${UNIT}")
}
"""

macros_exp1 = """
record(waveform, "$(P)$(R)Sensor4Wave"){
    field(DTYP, "asynFloat64ArrayIn")
    field(INP, "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s4.Wave")
    field(NELM, "$(ARCHIVER_SIZE)")
    field(FTVL, "DOUBLE")
    field(SCAN, "I/O Intr")
    field(DESC, "Sensor4 arch data")
    info(DESCRIPTION, "${BOARD_NAME} Sensor4 archiver waveform")
    field(EGU, "rpm")
}
"""

macros_exp2 = """
record(waveform, "$(P)${BOARD_TYPE}-$(ID)${OPT}Fan4Wave"){
    field(DTYP, "asynFloat64ArrayIn")
    field(INP, "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s4.Wave")
    field(NELM, "$(ARCHIVER_SIZE)")
    field(FTVL, "DOUBLE")
    field(SCAN, "I/O Intr")
    field(DESC, "Fan4 arch data")
    info(DESCRIPTION, "${BOARD_NAME} Fan4 archiver waveform")
    field(EGU, "rpm")
}
"""

input_macros1 = """# PVs for ${BOARD_NAME} board in MTCA chassis
record(longin, "$(P)${BOARD_TYPE}-$(ID)${OPT}SensorCnt"){
    field(DESC, "${BOARD_NAME} sensor counter")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).SensorCnt")
    field(SCAN, "I/O Intr")
    info(DESCRIPTION, "${BOARD_NAME} number of available sensors")
}
"""

input_macros2 = """# PVs for ${BOARD_NAME} board in MTCA chassis
record(longin, "$(P)$(R)SensorCnt"){
    field(DESC, "${BOARD_NAME} sensor counter")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).SensorCnt")
    field(SCAN, "I/O Intr")
    info(DESCRIPTION, "${BOARD_NAME} number of available sensors")
}
"""

board_exp1 = """# PVs for ${BOARD_NAME} board in MTCA chassis
record(longin, "$(P)${BOARD_TYPE}-$(ID)${OPT}SensorCnt"){
    field(DESC, "${BOARD_NAME} sensor counter")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).SensorCnt")
    field(SCAN, "I/O Intr")
    info(DESCRIPTION, "${BOARD_NAME} number of available sensors")
}

record(longin, "$(P)${BOARD_TYPE}-$(ID)${OPT}State"){
    field(DESC, "${BOARD_NAME} board state")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).State")
    field(SCAN, "I/O Intr")
    info(DESCRIPTION, "${BOARD_NAME} board M state")
}
"""

board_exp2 = """# PVs for ${BOARD_NAME} board in MTCA chassis
record(longin, "$(P)$(R)SensorCnt"){
    field(DESC, "${BOARD_NAME} sensor counter")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).SensorCnt")
    field(SCAN, "I/O Intr")
    info(DESCRIPTION, "${BOARD_NAME} number of available sensors")
}

record(longin, "$(P)$(R)State"){
    field(DESC, "${BOARD_NAME} board state")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).State")
    field(SCAN, "I/O Intr")
    info(DESCRIPTION, "${BOARD_NAME} board M state")
}
"""

board_exp3 = """
record(longin, "$(P)$(R)FruId"){
    field(DESC, "CCTAM900/412 FRU ID")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).FruId")
    field(SCAN, "I/O Intr")
    info(DESCRIPTION, "CCTAM900/412 FRU ID")
}
"""

board_sens_exp3 = """
record(stringin, "$(P)$(R)Sensor0Name"){
    field(DTYP, "asynOctetRead")
    field(INP, "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s0.Name")
    field(SCAN, "I/O Intr")
    field(DESC, "Sensor0 name")
    info(DESCRIPTION, "CCTAM900/412 Sensor0 name")
}
"""

board_exp4 = """
record(longin, "$(P)CU-$(ID)FruId"){
    field(DESC, "SchroffuTCACU FRU ID")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).FruId")
    field(SCAN, "I/O Intr")
    info(DESCRIPTION, "SchroffuTCACU FRU ID")
}
"""

board_sens_exp4 = """
record(stringin, "$(P)CU-$(ID)Fan4Name"){
    field(DTYP, "asynOctetRead")
    field(INP, "@asyn($(PORT),$(ADDR),$(TIMEOUT))s$(SLOT).s4.Name")
    field(SCAN, "I/O Intr")
    field(DESC, "Fan4 name")
    info(DESCRIPTION, "SchroffuTCACU Fan4 name")
}
"""
